/*Exercice #1*/

let nombreSaisi = 0;

while (nombreSaisi < 1 || nombreSaisi > 3) {
    nombreSaisi = parseInt(prompt("Saisir un nombre compris entre 1 et 3"));
}

/*Exercice #2*/

let nombreSaisi = 0;

while (nombreSaisi < 10 || nombreSaisi > 20) {
    nombreSaisi = parseInt(prompt("Saisir un nombre compris entre 10 et 20"));
    if (nombreSaisi < 10) {
        nombreSaisi = parseInt(prompt("C'est plus grand!"));
    } 
    if(nombreSaisi > 20) {
        nombreSaisi = parseInt(prompt("C'est plus petit!"));
    }
    alert("Bravo!");
}

/*Exercice #3*/
let nombreSaisi = parseInt(prompt("Veuillez saisir un nombre"));

for (let i = nombreSaisi + 1; i < nombreSaisi + 11; i++) {
    console.log(i)
}

/*Exercice #4*/
/*Voir exercice 3*/

/*Exercice #5*/

let nombreSaisi = parseInt(prompt("Veuillez saisir un nombre"));

for (i = 1; i < 11; i++) {
    console.log(`table de ${nombreSaisi}: ${nombreSaisi} * ${i} = ${nombreSaisi * i}`);
}

/*Exercice #6*/

let arr=[10, 20, 30]


let somme = 0;
for (let i = 0; i < arr.length; i++) {
    somme += arr[i]
}
return somme



/*Exercice #7*/
let arr = [20, 30, 40];
let valMax = 0;
let pos = 0;
let number = 0;


for (let i = 0; i < 20; i++) {
    number = i;
    let pr = parseInt(prompt(`Saisir le nombre numéro ${number + 1} : `));
    arr.push(pr);

    if (arr[i] > valMax) {
        valMax = arr[i];
        pos = i;
    }
}
console.log(`Le nombre le plus grand est ${valMax} et il a été saisi en ${pos}ème position.`);

/*Exercice #8*/
let arr = [];
let valMax = 0;
let pos = 0;
let isZero = false;


while (isZero !== true) {
    let pr = parseInt(prompt(`Rentrez un nombre:`));
    arr.push(pr)
    if (pr === 0) {

        for (let i = 0; i < arr.length; i++) {

            if (arr[i] > valMax) {
                valMax = arr[i];
                pos = i;
                isZero = true;
            }
        }
    }
}


console.log(`Le nombre le plus grand est ${valMax} et il a été rentré en ${pos + 1}eme position.`);

/*Exercice #9*/
let achats = [];
let isZero = false;
let isZero2 = false; 
let pr = 0
let pr2 =0
let remise = pr2 - achats.reduce(reducer)
let sommeAll = achats.reduce(reducer)
let remiseMod = remise
let billetDix = 0
let billetCinq = 0
let billetUn = 0


const reducer = (previousValue, currentValue) => previousValue + currentValue;

while (isZero !== true) {
    let pr = parseInt(prompt('Veuillez rentrer les prix des articles achetés:'));
    achats.push(pr)
    if (pr === 0) {
        isZero = true
    }
}

while (isZero2 !== true) {
    pr2 = parseInt(prompt('Veuillez payer s\'il vous plait :'));
    if (achats.reduce(reducer) < pr2) {
        isZero2 = true
    }
}

while (remiseMod >= 10) {
    remiseMod -= 10
    billetDix++
} while (remiseMod >= 5 && remiseMod < 10) {
    remiseMod -= 5
    billetCinq++
} while (remiseMod >= 1 && remiseMod < 5) {
    remiseMod -= 1
    billetUn++
}
console.log(`Le total est payé est de ${sommeAll}, Vous avez choisi de donner ${remise} et vous recevrez ${billetDix} billet de Dix, ${billetCinq} billet de cinq, ${billetUn} de un`)

